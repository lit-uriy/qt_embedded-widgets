INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
HEADERS += $$PWD/qtscrolldial.h \
           $$PWD/scrolldialpopup.h
SOURCES += $$PWD/qtscrolldial.cpp \
           $$PWD/scrolldialpopup.cpp

include($$PWD/../basicdialgauge/basicdialgauge.pri)
include($$PWD/../scrollwheel/scrollwheel.pri)


#========= Dependency ==============
#	--- Qt ---
greaterThan(QT_MAJOR_VERSION, 4): QT *= widgets
QT *= svg
