INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
SOURCES += $$PWD/qtsvgtoggleswitch.cpp
HEADERS += $$PWD/qtsvgtoggleswitch.h


#========= Dependency ==============
#	--- Qt ---
greaterThan(QT_MAJOR_VERSION, 4): QT *= widgets
QT *= svg
