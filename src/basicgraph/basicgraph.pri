INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
SOURCES += $$PWD/qtbasicgraph.cpp
HEADERS += $$PWD/qtbasicgraph.h


#========= Dependency ==============
#	--- Qt ---
greaterThan(QT_MAJOR_VERSION, 4): QT *= widgets
QT *= svg
