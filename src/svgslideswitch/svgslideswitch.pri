INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
SOURCES += $$PWD/qtsvgslideswitch.cpp
HEADERS += $$PWD/qtsvgslideswitch.h

#========= Dependency ==============
#	--- Qt ---
greaterThan(QT_MAJOR_VERSION, 4): QT *= widgets
QT *= svg
