INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
HEADERS += $$PWD/qtmultislider.h
SOURCES += $$PWD/qtmultislider.cpp


#========= Dependency ==============
#	--- Qt ---
greaterThan(QT_MAJOR_VERSION, 4): QT *= widgets
QT *= svg
