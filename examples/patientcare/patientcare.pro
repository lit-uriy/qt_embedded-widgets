TEMPLATE = app
#INCLUDEPATH += .

include($$PWD/../../src/common/common.pri)
include($$PWD/../../src/svgslideswitch/svgslideswitch.pri)
include($$PWD/../../src/scrolldial/scrolldial.pri)
include($$PWD/../../src/5waybutton/5waybutton.pri)
include($$PWD/../../src/multislider/multislider.pri)
include($$PWD/../../src/basicgraph/basicgraph.pri)
include($$PWD/../../src/svgtoggleswitch/svgtoggleswitch.pri)


RESOURCES += $$PWD/patientcare.qrc \
    $$PWD/../../skins/beryl_multislider.qrc \
    $$PWD/../../skins/beryl_svgslideswitch.qrc \
    $$PWD/../../skins/beryl_scrolldial.qrc \
    $$PWD/../../skins/beryl_5waybutton.qrc \
    $$PWD/../../skins/beryl_scrollwheel.qrc \
    $$PWD/../../skins/beryl_svgtoggleswitch.qrc


HEADERS += \
    $$PWD/mainwindow.h \
    $$PWD/datagenerator.h \
    $$PWD/patientcarecontroller.h \
    $$PWD/lazytimer.h

SOURCES += \
    $$PWD/main.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/datagenerator.cpp \
    $$PWD/patientcarecontroller.cpp \
    $$PWD/lazytimer.cpp

FORMS = $$PWD/patientcare.ui



#========= Dependency ==============
#	--- Qt ---
QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT *= svg


#====== Convenient config =========
DEBUGCONFIG = false
include($$(SOFT_LIB)/myPri/stdconfig.pri)
