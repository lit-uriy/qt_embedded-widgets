TEMPLATE = app
#INCLUDEPATH += .

include($$PWD/../../src/common/common.pri)
include($$PWD/../../src/5waybutton/5waybutton.pri)
include($$PWD/../../src/basicgraph/basicgraph.pri)
include($$PWD/../../src/multislider/multislider.pri)
include($$PWD/../../src/scrolldial/scrolldial.pri)
# omit scrollwheel, it's already included in scrolldial.pri
include($$PWD/../../src/svgbutton/svgbutton.pri)
include($$PWD/../../src/svgdialgauge/svgdialgauge.pri)
include($$PWD/../../src/svgslideswitch/svgslideswitch.pri)
include($$PWD/../../src/svgtoggleswitch/svgtoggleswitch.pri)


# Input
HEADERS += \
    $$PWD/mainwindow.h \


SOURCES += \
    $$PWD/main.cpp \
    $$PWD/mainwindow.cpp \


RESOURCES += \
    $$PWD/../../skins/thermometer_svgdialgauge.qrc \
    $$PWD/../../skins/tachometer_svgdialgauge.qrc \
    $$PWD/../../skins/amperemeter_svgdialgauge.qrc \
    $$PWD/../../skins/beryl_5waybutton.qrc \
    $$PWD/../../skins/beryl_multislider.qrc \
    $$PWD/../../skins/beryl_svgslideswitch.qrc \
    $$PWD/../../skins/beryl_svgbutton.qrc \
    $$PWD/../../skins/beryl_svgtoggleswitch.qrc \
    $$PWD/../../skins/berylsquare_svgtoggleswitch.qrc \
    $$PWD/../../skins/berylsquare_svgbutton.qrc \
    $$PWD/../../skins/beryl_scrollwheel.qrc \
    $$PWD/../../skins/beryl_scrolldial.qrc \
    $$PWD/../../skins/metallicbrush_svgbutton.qrc \
    $$PWD/../../skins/metallicbrush_svgslideswitch.qrc

RESOURCES += $$PWD/catalog.qrc





#========= Dependency ==============
#	--- Qt ---
QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT *= svg


#====== Convenient config =========
DEBUGCONFIG = false
include($$(SOFT_LIB)/myPri/stdconfig.pri)
